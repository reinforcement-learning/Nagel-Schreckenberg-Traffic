# Number of cars
nb_cars = 100

#Number of spaces / slots
nb_spaces = 300

# Maximum speed
max_speed = 5

# probability : probability of random slowing
probability = 1/3

# Number of simulations
nb_simulations = 100