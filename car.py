import math
import random
import config

class Car:
    #Constructor
    def __init__(self, _init_position, _init_speed, _headway):
        self.position = _init_position
        self.speed = _init_speed
        self.headway = _headway

    def UpdatePosition(self):
        self.position = self.position + self.speed
        if self.position >= config.nb_spaces:
            self.position = self.position - config.nb_spaces
        return self.position

    def UpdateSpeed(self):
        self.speed = min(self.speed + 1, config.max_speed)
        self.speed = min(self.speed, self.headway - 1)

        sample = random.random()
        if sample < config.probability:
            self.speed = max(0, self.speed - 1)
