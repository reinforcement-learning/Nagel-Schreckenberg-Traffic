# Monte Carlo simulation of the Nagel- Schreckenberg traffic model


Monte Carlo methods have proven useful in studying how vehicle traffic behaves. Perhaps the most interesting aspect of traffic is the occurrence of traffic jams. At places where the number of traffic lanes is reduced, cars slow down and form a blockage. Similarly, accidents or poor visibility or the occasional slow vehicle can bring about a traffic jam.

Sometimes, however, a traffic jam has no apparent cause. It just spon- taneously appears in flowing traffic, and moves slowly backwards against the traffic. It can last a long time and then simply disappear.

The phenomenon can be illustrated with Monte Carlo methods. A very simple Monte Carlo simulation that captures some of the important properties of real traffic is the Nagel-Schreckenberg model. In this model the roadway is divided up into *M* distinct zones, each of which can hold one vehicle. There are *N* vehicles in the road. Time moves forward in discrete steps. A vehicle with velocity *v* moves ahead by *v* zones in the roadway at the next time step. There is a maximum speed *v<sub>max</sub>* which all vehicles obey. In the simplest case, the roadway is a circular loop.

The rules for Nagel-Schreckenberg traffic are as follows. At each stage of the simulation, every car goes through the following 4 steps. First, if its velocity is below *v<sub>max</sub>*, it increases its velocity by one unit. The drivers are eager to move ahead. Second, it checks the distance to the car in front of it. If that distance is *d* spaces and the car has velocity *v* >= *d* then it reduces its velocity to *d* − 1 in order to avoid collision. Third, if the velocity is positive then with probability *p* it reduces velocity by 1 unit. This is the key step which models random driver behavior. At the fourth step, the car moves ahead by *v* units to complete the stage. These four steps take place in parallel for all *N* vehicles.

Let *x* ∈ {0,1,...,*M* − 1} be the position of a car, *v* its velocity, and *d* be the distance to the car ahead. The update for this car is:

*v* ← min(*v* + 1, *v<sub>max</sub>*)

*v* ← min(*v*, *d* − 1)

*v* ← max(0, *v* − 1) with probability *p*

*x* ← *x* + *v*.

At the last step,if *x* + *v* >= *M* then *x* ← *x* + *v* − *M*. Similarly, for the car with the largest *x*, the value of *d* is *M* plus the smallest *x*, minus the largest *x*.

Figure 1 shows a sample realization of this process. It has *N* = 100 vehicles in a circular track of *M* = 1000 spaces. The speed limit is *v<sub>max</sub>* = 5 and the probability of random slowing is *p* = 1/3.

![Figure 1: Nagel−Schreckenberg traffic: 100 cars, 1000 spaces](Tests/NST_1000spaces_100cars.png)

This figure illustrates a Monte Carlo simulation of the Nagel-Schreckenberg traffic model as described in the text. The simulation starts with 100 cars represented as black dots on the top row. The vehicles move from left to right, as time increases from top to bottom. Traffic jams emerge spontaneously. They show up as dark diagonal bands. The traffic jams move backward as the traffic moves forward.

Figure 2 illustrates a Monte-Carlo simulation of the Nagel-Schreckenberg traffic model with N = 100 vehicles and M = 300 spaces. The speed limit is *v<sub>max</sub>* = 5 and the probability of random slowing is *p* = 1/3.

![Figure 2: Nagel−Schreckenberg traffic: 100 cars, 300 spaces](Tests/NST_300spaces_100cars.png)



