import random
import numpy as np
import config
from car import Car
from matplotlib import pyplot as plt

class Main:
    # Constructor
    # _probability : probability of random slowing
    def __init__(self, _nb_cars, _nb_spaces, _max_speed, _probability):
        self.nb_cars = _nb_cars
        self.nb_spaces = _nb_spaces
        self.max_speed = _max_speed
        self.probability = _probability
        self.init_speed = 0

        self.cars = []
        self.simulations = []

    def Initialize(self, to_print = True):
        self.spaces = np.arange(self.nb_spaces)

        # Return a k length list of unique elements chosen from the population sequence. Used for random sampling without replacement.
        positions = random.sample(self.spaces, self.nb_cars)
        positions = np.sort(positions)
        self.simulations.append(positions)

        for i in range(0, self.nb_cars - 1):
            headway = positions[i+1] - positions[i]
            car = Car(positions[i], self.init_speed, headway)
            self.cars.append(car)

        # For the car with the largest x
        headway = self.nb_spaces - positions[-1] + positions[0]
        car = Car(positions[-1], self.init_speed, headway)
        self.cars.append(car)

        if to_print:
            print("Spaces", self.spaces)

    def Simulate(self, nb_simulations):
        for i in range(0, nb_simulations):
            positions = []
            for j in range(0, self.nb_cars):
                self.cars[j].UpdateSpeed()
                position = self.cars[j].UpdatePosition()
                positions.append(position)

            self.simulations.append(positions)

            # Update headway
            smallest_x = min(positions)
            largest_x = max(positions)

            for j in range(0, self.nb_cars - 1):
                if self.cars[j].position == largest_x:
                    self.cars[j].headway = self.nb_spaces - largest_x + smallest_x
                else:
                    self.cars[j].headway = positions[j+1] - positions[j]

            # For the last car on the list
            if self.cars[-1].position == largest_x:
                self.cars[-1].headway = self.nb_spaces - largest_x + smallest_x
            else:
                self.cars[-1].headway = positions[0] - positions[-1]

main = Main(config.nb_cars, config.nb_spaces, config.max_speed, config.probability)
main.Initialize()
main.Simulate(config.nb_simulations)

plt.figure(figsize = (10,10))
plt.xlabel('Distance')
plt.ylabel('Time')
plt.title('Nagel-Schreckenberg traffic')
plt.axis([0, config.nb_spaces, config.nb_simulations, 0])

for i in range(0, len(main.simulations)):
    time = np.ones(config.nb_cars, dtype=int)
    time *= i
    line, = plt.plot(main.simulations[i], time, 'ro')

plt.show()

